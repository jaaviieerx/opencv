from multiprocessing import Pool, cpu_count
from imutils import paths
from invert import inverter, chunk
import numpy as np
import argparse
from timeit import default_timer as timer

if __name__ == "__main__":
	print("program initialised")
	# to intake arguments on execution via terminal
	start1 = timer()
	argp = argparse.ArgumentParser()
	argp.add_argument("-i", "--input", required=True, type=str,
		help="input path for images")
	argp.add_argument("-o", "--output", required=True, type=str,
		help="output path to store files")
	argp.add_argument("-p", "--procs", default=1, type=int,
		help="no. of processes to run")
	args = vars(argp.parse_args())
	# list PIDs
	procs = args["procs"] if args["procs"] > 0 else cpu_count()
	procIDs = list(range(0, procs))
	# calculate total images to each process, then chunk
	allInputPaths = sorted(list(paths.list_images(args["input"])))
	imgPerProc = len(allInputPaths)/float(procs)
	imgPerProc = int(np.ceil(imgPerProc))
	chunkPaths = list(chunk(allInputPaths, imgPerProc))
	print("input paths fetched with {} images per process".format(imgPerProc))
    # payloads initialisation for chunks
	payloads = []
	for (l, inputPath) in enumerate(chunkPaths):
		data = {
			"id": l,
			"input_path": inputPath,
			"output_path": args["output"]
		}
		payloads.append(data)

	# parallel multiprocessing via pool
	print("launching pool using {} processes...".format(procs))
	pool = Pool(processes=procs)
	pool.map(inverter, payloads)
	pool.close()
	pool.join()
	end1 = timer()
	print("program exited in {0:.2f} seconds".format(end1-start1))

# $ python3 main.py -i sample3 -o output -p 1