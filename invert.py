import numpy as np
import cv2, os
from timeit import default_timer as timer

def chunk(l, n):
	# loop across N-size chunks
	for i in range(0, len(l), n):
		# for calling function
		yield l[i: i + n]

def inverter(payload):
	start2 = timer()
	print("process {} starting".format(payload["id"]))
	# loop over each image
	for imagePath in payload["input_path"]:
		# load sample image, invert it, save it
		image = cv2.imread(imagePath)
		not_img = cv2.bitwise_not(image)
		basename = os.path.basename(imagePath)
		filename = "{}/{}".format(payload["output_path"], basename)
		save_img = cv2.imwrite(filename, not_img)
	end2 = timer()
	print("process {} completed in".format(payload["id"]), "{0:.2f} seconds".format(end2-start2))
