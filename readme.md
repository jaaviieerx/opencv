## Introduction
* Testing OpenCV on Python Multiprocessing
* Map chunk of images to equal number of processes to each processor via Pool
* Measure time of parallelism via Timeit
* To apply negative filter 